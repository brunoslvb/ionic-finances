import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HeaderLoginComponent } from './components/header-login/header-login.component';
import { ForgotPage } from './forgot/forgot.page';
import { LoginPage } from './login/login.page';
import { RegisterPage } from './register/register.page';

const routes: Routes = [
  { path: '', children: [
    { path: '', component: LoginPage },
    { path: 'register', component: RegisterPage },
    { path: 'forgot', component: ForgotPage },
  ] }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoginPage,
    RegisterPage,
    ForgotPage,
    HeaderLoginComponent
  ]
})
export class AuthRoutingModule { }
