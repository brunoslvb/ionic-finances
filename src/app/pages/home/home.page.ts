import { Component } from '@angular/core';
import { LoginService } from '../auth/service/login.service';
import { ContaService } from '../contas/service/conta.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  date = new Date().toISOString();

  conta: any = {
    pagar: {num: 0, valor: 0 },
    receber: {num: 0, valor: 0 },
    saldo: {num: 0, valor: 0 }
  };

  constructor(
    private service: LoginService,
    private contaService: ContaService
  ) {}

  ionViewWillEnter(){
    this.atualizaContas();
  }
  
  async atualizaContas(){
    this.conta.pagar = await this.contaService.total('pagar', this.date);
    this.conta.receber = await this.contaService.total('receber', this.date);
    this.atualizaSaldo();
  }

  atualizaSaldo(){
    this.conta.saldo.num = this.conta.pagar.num + this.conta.receber.num;
    this.conta.saldo.valor = this.conta.receber.valor - this.conta.pagar.valor;
  }

  logout(){
    this.service.logout();
  }

}
