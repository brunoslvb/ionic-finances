import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { DateHelper } from 'src/app/helpers/DateHelper';
import { ContaService } from '../service/conta.service';

@Component({
  selector: 'cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  contasForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private nav: NavController,
    private contaService: ContaService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm(){
    this.contasForm = this.builder.group({
      parceiro: ['', [Validators.required, Validators.minLength(5)]],
      descricao: ['', [Validators.required, Validators.minLength(6)]],
      valor: ['', [Validators.required, Validators.min(0.01)]],
      tipo: ['', Validators.required],
      date: [new Date().toISOString(), Validators.required]
    });
  }

  registraConta() {
    let conta = this.contasForm.value;
    const date = this.contasForm.get('date').value;

    conta = {...conta, ...DateHelper.breakDate(date)};
    
    delete conta.date;

    this.contaService.registraConta(conta).then(() => this.nav.navigateForward(`/contas/${this.contasForm.value.tipo}`));
  }

}
