import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ContaService } from '../service/conta.service';

@Component({
  selector: 'lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {

  contasForm: FormGroup;

  listaContas;

  tipo: string;

  constructor(
    private contaService: ContaService,
    private alert: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
    this.tipo = this.router.url.split('/')[2];
    this.contaService.lista(this.tipo).subscribe(x => this.listaContas = x);
  }

  async remove(conta){

    const confirm = await this.alert.create({
      header: "Remover conta",
      message: "Deseja realmente deletar essa conta?",
      buttons: [
        {
          text: "Cancelar",
          role: 'cancel',
        },
        {
          text: 'Deletar',
          handler: () => this.contaService.remove(conta)
        }
      ]
    });

    await confirm.present();

  }

  async edita(conta){

    const confirm = await this.alert.create({
      header: "Editar conta",
      inputs: [
        {
          name: 'parceiro',
          placeholder: 'Parceiro Comercial',
          value: conta.parceiro
        },
        {
          name: 'descricao',
          placeholder: 'Descrição da conta',
          value: conta.descricao
        },
        {
          name: 'valor',
          type: 'number',
          placeholder: 'Valor da conta',
          value: conta.valor
        }
      ],
      buttons: [
        {
          text: "Cancelar",
          role: 'cancel',
        },
        {
          text: 'Editar',
          handler: (data) => {
            this.contaService.edita({...conta, ...data})
          }
        }
      ]
    });

    await confirm.present();
  }

}
