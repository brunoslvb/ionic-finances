import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastroPage } from './cadastro/cadastro.page';
import { RelatorioPage } from './relatorio/relatorio.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListaPage } from './lista/lista.page';

const routes: Routes = [
  { path: '', children: [
    { path: 'pagar', component: ListaPage },
    { path: 'receber', component: ListaPage },
    { path: 'cadastro', component: CadastroPage },
    { path: 'relatorio', component: RelatorioPage },
  ] },
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    CadastroPage,
    RelatorioPage,
    ListaPage
  ]
})
export class ContasRoutingModule { }
