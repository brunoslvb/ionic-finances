import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { DateHelper } from 'src/app/helpers/DateHelper';

@Injectable({
  providedIn: 'root'
})
export class ContaService {

  collection: AngularFirestoreCollection;

  constructor(
    private db: AngularFirestore
  ) { }

  registraConta(conta) {
    conta.id = this.db.createId();
    this.collection = this.db.collection('conta');
    return this.collection.doc(conta.id).set(conta);
  }

  lista(tipo){
    this.collection = this.db.collection('conta', ref => ref.where('tipo', '==', tipo));
    return this.collection.valueChanges();
  }

  remove(conta){
    this.collection = this.db.collection('conta');
    return this.collection.doc(conta.id).delete();
  }

  edita(conta) {
    this.collection = this.db.collection('conta');
    return this.collection.doc(conta.id).update(conta);
  }

  total(tipo: string, date) {

    const aux = DateHelper.breakDate(date);

    return new Promise((resolve) => {

      this.collection = this.db.collection('conta', ref => ref.where('tipo', '==', tipo).where('mes', '==', aux.mes).where('ano', '==', aux.ano));
      
      this.collection.get().pipe(map(snap => {
        let count = 0;
        let sum = 0;
        snap.docs.map(doc => {
          const conta  = doc.data();
          const valor = parseFloat(conta.valor);
          sum += valor;
          count++;
        });

        return { num: count, valor: sum };

      })).subscribe(x => resolve(x));
    });
  }

}


