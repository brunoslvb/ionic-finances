export class DateHelper {

  static breakDate(date) {
    const aux: Array<any> = date.substring(0, 10).split('-').map(item => {
      return parseInt(item);
    });

    return {
      dia: aux[2],
      mes: aux[1],
      ano: aux[0]
    }
  }

}